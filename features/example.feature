Feature: StringCalculator
  Calculate addition of string integers

  Scenario: Base case
    When I provide "2" and add it
    Then I want it to be 2
  Scenario: Test case 1
    When I provide "" and add it
    Then I want it to be 0
  Scenario: Test case 2
    When I provide "2,3" and add it
    Then I want it to be 5