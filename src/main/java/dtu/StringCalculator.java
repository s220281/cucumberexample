package dtu;

public class StringCalculator {
    /**
     * @param a
     * @param b
     * @return
     */
    public int add(String numbers) {
        if(numbers.isEmpty()) {
            return 0;
        }
        String [] tokens = numbers.split(",");
        int sum = 0;
        for(int i = 0;  i < tokens.length; i++) {
            sum += Integer.parseInt(tokens[i]);
        }
        return sum;
    }
}
