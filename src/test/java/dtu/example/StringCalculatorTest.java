package dtu.example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import dtu.StringCalculator;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StringCalculatorTest {
    int result;
    // @Given("This is to add numbers from a string separated by commas")
    // void testGiven(){
    //     //
    // }

    @When("I provide {string} and add it")
    public void testAdd(String numbers) {
        var sc = new StringCalculator();
        result = sc.add(numbers);
    }
    @Then("I want it to be {int}")
    public void testAsset(Integer expected) {
        assertEquals(expected, result);
    }

}
